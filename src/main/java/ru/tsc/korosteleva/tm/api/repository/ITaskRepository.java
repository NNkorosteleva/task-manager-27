package ru.tsc.korosteleva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Task create(@NotNull String userId,
                @NotNull String name,
                @NotNull String description,
                @NotNull Date dateBegin,
                @NotNull Date dateEnd);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task updateById(@NotNull String userId,
                    @NotNull String id,
                    @NotNull String name,
                    @NotNull String description);

    @Nullable
    Task updateByIndex(@NotNull String userId,
                       @NotNull Integer index,
                       @NotNull String name,
                       @NotNull String description);

    @Nullable
    Task removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

}