package ru.tsc.korosteleva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name,
                @Nullable String description,
                @Nullable Date dateBegin,
                @Nullable Date dateEnd);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task findOneByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateById(@Nullable String userId,
                    @Nullable String id,
                    @Nullable String name,
                    @Nullable String description);

    @NotNull
    Task updateByIndex(@Nullable String userId,
                       @Nullable Integer index,
                       @Nullable String name,
                       @Nullable String description);

    @NotNull
    Task removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}