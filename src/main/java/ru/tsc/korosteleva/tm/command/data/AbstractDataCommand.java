package ru.tsc.korosteleva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.service.IAuthService;
import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.api.service.ITaskService;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.command.AbstractCommand;
import ru.tsc.korosteleva.tm.dto.Domain;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    public IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setUsers(getUserService().findAll());
        domain.setProjects(getProjectService().findAll());
        domain.setTasks(getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        getUserService().set(domain.getUsers());
        getProjectService().set(domain.getProjects());
        getTaskService().set(domain.getTasks());
        getAuthService().logout();
    }

}
